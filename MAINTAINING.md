# ...

Setup tools:

```bash
python3 -m pip install --user --upgrade pipx
pipx install poetry
pipx upgrade poetry
```

Run:

```bash
poetry install
poetry run pyld-xtl --help
poetry run pyld-xtl -i ~/d.sws/icat/rdf/foaf.ttl
poetry run pyld-xtl -vvv normalize --url https://raw.githubusercontent.com/json-ld/json-ld.org/master/examples/syntax/example-061-Aliasing-keywords.json
poetry run pyld-xtl -vvv normalize ~/d.x/github.com/json-ld.org/examples/syntax/example-061-Aliasing-keywords.json
```

```bash
rdfpipe -i turtle -o json-ld ~/d/dev.azure.com/pdgm/BigLoop/incubator/rdf/emerson-bigloop.owl.ttl | poetry run pyld-xtl -vvv compact --of YAML --context ~/d/dev.azure.com/pdgm/BigLoop/incubator/rdf/context.ld.yaml /dev/stdin
poetry run pyld-xtl -vvv flatten --of YAML --context ~/d/dev.azure.com/pdgm/BigLoop/incubator/rdf/context.ld.yaml ~/d/dev.azure.com/pdgm/BigLoop/incubator/rdf/emerson-bigloop.owl.ld.yaml | poetry run pyld-xtl -vvv compact --of YAML --context ~/d/dev.azure.com/pdgm/BigLoop/incubator/rdf/context.ld.yaml /dev/stdin
```

```bash
rm -rf dist/
poetry build
pip3 install --user --upgrade $(ls -tr dist/*.whl | tail -1)
```

Validate:

```bash
poetry run pylint ./src ./tests
poetry run pytest ./tests
poetry run mypy --strict ./src ./tests
```

Publish

```bash
# Get and set token:
# https://pypi.org/manage/account/token/
poetry config pypi-token.pypi ...

# ...
git commit -m "prerelease checkpoint" .; git push
poetry publish --build
poetry version prerelease
find src -name '__init__.py' -print0 | xargs -0 sed -i -e 's|^.*__version__.*$|__version__ =  "'"$(poetry version | gawk '{ print $2 }')"'"|g'
git commit -m "prerelease checkpoint" .; git push
poetry install

# ...
poetry version minor
poetry build
git status
git commit -m "preparing for versioning" .; git push
git tag "v$(poetry version | gawk '{ print $2 }')"
git push --tags
poetry publish --build
poetry version preminor
sed -i -e 's|^.*__version__.*$|__version__ =  "'"$(poetry version | gawk '{ print $2 }')"'"|g' src/yocho/rdflib_xtl/__init__.py
git commit -m "post versioning" .; git push
poetry install
```

## Other stuff

```
poetry cache clear --all .
\rm -vr /home/iwana/.cache/pypoetry/virtualenvs/
find $(dirname $(poetry run which python))/../lib/*/site-packages/ | grep egg-link
echo "$(dirname -- "$(dirname -- "$(poetry run which python)")")"
\rm -vr "$(dirname -- "$(dirname -- "$(poetry run which python)")")"

find . -depth \( -name '*.egg-info' -o -name '__pycache__' -o -name '*.pyc' \) -print0 | xargs -0 rm -vr

poetry install -vvv
PYTHONPATH="$(pwd)/src/${PYTHONPATH:+:${PYTHONPATH}}" poetry run pylint src/
```

## CMDs

```
printf "%s\000" ~/d.x/opengroup.org/community/osdu/platform/open-test-data/rc--2.0.0/4-instances/from_r1/master-data/Well/load_well_100*.json | xargs -0 -I{} poetry run pyld-xtl flatten --inject-context ~/d/gitlab.com/aucampia/incubator/osdu-ld/osdu-xidei.ctx.ld.yaml {} --of NQUADS
```
